﻿ImplementationGuideSaudeiD : Validation Results
=========================================

err = 0, warn = 0, info = 39
IG Publisher Version: 1.1.90 Out of date - current version is ?pub-ver-1?
Generated Mon Dec 20 17:41:58 BRST 2021. FHIR version 4.0.1 for saudeid#0.0.2 (canonical = http://saudeid.com.br/fhir)
 Build Errors : 0 / 0 / 0
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\examples\patient-example : 0 / 0 / 2
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\codesystem-cbo-saudeid : 0 / 0 / 5
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-agreedWithUserTerms : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-biologicalSex : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-deviceOperationalSystem : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-ethnicity : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-fathersName : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-idClienteFleury : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-idDocumentoFleury : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-idWorkstation : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-isB2B : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-isB2C : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-lastUpdate : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-mothersName : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-occupation : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-extension-patient-prontmedCode : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\structuredefinition-patient-saudeid : 0 / 0 / 3
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\resources\valueset-cbo-saudeid : 0 / 0 / 4
 C:\Users\SaudeiD\Documents\git\saudeid-ig-fhir\input\saudeid-ig : 0 / 0 / 3
</table>
== n/a ==
== input\examples\patient-example.json ==
== input\resources\codesystem-cbo-saudeid.json ==
INFORMATION: CodeSystem/CBO: CodeSystem.language: The value provided ('pt-BR') is not in the value set 'Common Languages' (http://hl7.org/fhir/ValueSet/languages), and a code is recommended to come from this value set) (error message = Attempt to use Terminology server when no Terminology server is available)
INFORMATION: CodeSystem/CBO: CodeSystem.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: CodeSystem/CBO: CodeSystem.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
INFORMATION: CodeSystem/CBO: CodeSystem: CodeSystems SHOULD have a stated value for the caseSensitive element so that users know the status and meaning of the code system clearly
== input\resources\structuredefinition-extension-patient-agreedWithUserTerms.json ==
INFORMATION: StructureDefinition/patient-agreedWithUserTerms: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-agreedWithUserTerms: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-biologicalSex.json ==
INFORMATION: StructureDefinition/patient-biologicalSex: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-biologicalSex: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-deviceOperationalSystem.json ==
INFORMATION: StructureDefinition/patient-deviceOperationalSystem: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-deviceOperationalSystem: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-ethnicity.json ==
INFORMATION: StructureDefinition/patient-ethnicity: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-ethnicity: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-fathersName.json ==
INFORMATION: StructureDefinition/patient-fathersName: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-fathersName: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-idClienteFleury.json ==
INFORMATION: StructureDefinition/patient-idClienteFleury: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-idClienteFleury: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-idDocumentoFleury.json ==
INFORMATION: StructureDefinition/patient-idDocumentoFleury: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-idDocumentoFleury: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-idWorkstation.json ==
INFORMATION: StructureDefinition/patient-idWorkstation: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-idWorkstation: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-isB2B.json ==
INFORMATION: StructureDefinition/patient-isB2B: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-isB2B: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-isB2C.json ==
INFORMATION: StructureDefinition/patient-isB2C: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-isB2C: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-lastUpdate.json ==
INFORMATION: StructureDefinition/patient-lastUpdate: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-lastUpdate: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-mothersName.json ==
INFORMATION: StructureDefinition/patient-mothersName: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-mothersName: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-occupation.json ==
INFORMATION: StructureDefinition/patient-occupation: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-occupation: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-extension-patient-prontmedCode.json ==
INFORMATION: StructureDefinition/patient-prontmedCode: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/patient-prontmedCode: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\structuredefinition-patient-saudeid.json ==
INFORMATION: StructureDefinition/Patient-SaudeiD: StructureDefinition.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: StructureDefinition/Patient-SaudeiD: StructureDefinition.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\resources\valueset-cbo-saudeid.json ==
INFORMATION: ValueSet/CBO-1.0: ValueSet.language: The value provided ('pt-BR') is not in the value set 'Common Languages' (http://hl7.org/fhir/ValueSet/languages), and a code is recommended to come from this value set) (error message = Attempt to use Terminology server when no Terminology server is available)
INFORMATION: ValueSet/CBO-1.0: ValueSet.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: ValueSet/CBO-1.0: ValueSet.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
== input\saudeid-ig.xml ==
INFORMATION: ImplementationGuide/saudeid: ImplementationGuide.jurisdiction[0]: Could not confirm that the codes provided are from the extensible value set http://hl7.org/fhir/ValueSet/jurisdiction because there is no terminology service
INFORMATION: ImplementationGuide/saudeid: ImplementationGuide.jurisdiction[0].coding[0]: Code System URI 'http://unstats.un.org/unsd/methods/m49/m49.htm' is unknown so the code cannot be validated
